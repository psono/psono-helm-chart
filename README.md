# Psono Helm Chart

This Helm chart facilitates the deployment of Psono, a secure password manager, within a Kubernetes cluster.

## Prerequisites

- Kubernetes cluster (version 1.20 or higher recommended)
- Helm 3
- Docker (for generating server keys)
- Configured database for persistent storage

## Installation Steps

### 1. Clone the Repository

Begin by cloning the Psono Helm chart repository:

```bash
git clone https://gitlab.com/psono/psono-helm-chart.git
cd psono-helm-chart
```

### 2. Generate Server Keys

Psono requires specific server keys for operation. Generate these keys using Docker:

```bash
docker run --rm -ti psono/psono-server:latest python3 ./psono/manage.py generateserverkeys
```

Securely store the generated keys, as they will be needed in subsequent steps.

### 3. Configure `values.yaml`

Edit the `values.yaml` file to include your generated keys and database credentials:

```yaml
service:
  env:
    PSONO_SECRET_KEY: '<your-secret-key>'
    PSONO_ACTIVATION_LINK_SECRET: '<your-activation-link-secret>'
    PSONO_DB_SECRET: '<your-db-secret>'
    PSONO_EMAIL_SECRET_SALT: '<your-email-secret-salt>'
    PSONO_PRIVATE_KEY: '<your-private-key>'
    PSONO_PUBLIC_KEY: '<your-public-key>'
    PSONO_DATABASES_DEFAULT_NAME: '<database-name>'
    PSONO_DATABASES_DEFAULT_USER: '<database-user>'
    PSONO_DATABASES_DEFAULT_PASSWORD: '<database-password>'
    PSONO_DATABASES_DEFAULT_HOST: '<database-host>'
```

Ensure all placeholders are replaced with your actual configuration details.


### 4. Using Kubernetes Secrets for Environment Variables

For enhanced security and management of sensitive data, you can reference Kubernetes Secrets in the `envFrom` field, instead of directly specifying secrets in `values.yaml`. This approach is especially useful when managing secrets in external systems or securely rotating them without modifying the `values.yaml` file directly.

### Configure Secrets with `envFrom`

1. **Create a Kubernetes Secret**: First, create a Kubernetes Secret containing the necessary environment variables for Psono.

   ```bash
   kubectl create secret generic psono-secrets \
       --from-literal=PSONO_SECRET_KEY='<your-secret-key>' \
       --from-literal=PSONO_ACTIVATION_LINK_SECRET='<your-activation-link-secret>' \
       --from-literal=PSONO_DB_SECRET='<your-db-secret>' \
       --from-literal=PSONO_EMAIL_SECRET_SALT='<your-email-secret-salt>' \
       --from-literal=PSONO_PRIVATE_KEY='<your-private-key>' \
       --from-literal=PSONO_PUBLIC_KEY='<your-public-key>' \
       --from-literal=PSONO_DATABASES_DEFAULT_NAME='<database-name>' \
       --from-literal=PSONO_DATABASES_DEFAULT_USER='<database-user>' \
       --from-literal=PSONO_DATABASES_DEFAULT_PASSWORD='<database-password>' \
       --from-literal=PSONO_DATABASES_DEFAULT_HOST='<database-host>'
   ```

   Ensure each placeholder (`<your-secret-key>`, etc.) is replaced with the appropriate value.

2. **Reference the Secret in `values.yaml`**: Update your `values.yaml` to use `envFrom` instead of individual environment variables.

   ```yaml
   service:
     envFrom:
       - secretRef:
           name: psono-secrets
   ```

   This configuration instructs Kubernetes to load all key-value pairs from the `psono-secrets` secret as environment variables within the Psono container.

3. **Deploy or Update Psono**: Use the Helm install or upgrade command to deploy Psono with this configuration.

   ```bash
   helm install psono ./psono-helm-chart
   ```

   If you're updating an existing release, use:

   ```bash
   helm upgrade psono ./psono-helm-chart
   ```

### Example `values.yaml` for `envFrom`

Here's a simplified example of how `envFrom` could look within `values.yaml`:

```yaml
service:
  envFrom:
    - secretRef:
        name: psono-secrets
```

By referencing the secret through `envFrom`, sensitive information is managed directly in Kubernetes Secrets, enhancing security and simplifying configuration management.

### 5. Deploy Psono

Install the Helm chart with a release name of your choice (e.g., `psono`):

```bash
helm install psono ./psono-helm-chart
```

This command deploys Psono using the configurations specified in `values.yaml`.

### 5. Verify Deployment

Check the status of your pods to ensure successful deployment:

```bash
kubectl get pods
```

All Psono-related pods should display a `Running` status.

## Configuration Options

The `values.yaml` file offers various configuration options:

- **Image Settings**:
    - `repository`: Specify the Docker image repository. For the Enterprise edition, use `psono/psono-combo-enterprise`.
    - `tag`: Define the image tag. Adjust accordingly if using the Enterprise edition.
    - `pullPolicy`: Set the image pull policy (e.g., `IfNotPresent`).

- **Service Account**:
    - `create`: Determine whether to create a new service account.
    - `annotations`: Add any necessary annotations.
    - `name`: Specify the service account name.

- **Service Configuration**:
    - `type`: Define the service type (`ClusterIP`, `NodePort`, or `LoadBalancer`).
    - `port`: Set the service port.

- **Ingress Settings**:
    - `enabled`: Enable or disable ingress.
    - `annotations`: Add ingress annotations.
    - `hosts`: List the hostnames.
    - `tls`: Configure TLS settings.

- **Resource Management**:
    - Define resource `limits` and `requests` as needed.

- **Autoscaling**:
    - Configure horizontal pod autoscaling parameters.

- **Node Affinity and Tolerations**:
    - Set node selectors, tolerations, and affinity rules.

For a comprehensive list of configuration options, refer to the `values.yaml` file in the repository.

## Uninstallation

To remove the Psono deployment:

```bash
helm uninstall psono
```

This command deletes all Kubernetes resources associated with the `psono` release.

## Additional Resources

- [Psono Documentation](https://doc.psono.com/)
- [Kubernetes Secrets Management](https://kubernetes.io/docs/concepts/configuration/secret/)

For further assistance, consult the Psono documentation or seek support from the community. 